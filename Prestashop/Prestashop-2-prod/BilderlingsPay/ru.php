<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_c8d1b39a820aeb9115fb35f0cfdef06a'] = 'BilderlingsPay';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_08d066c15fc2b7f439c9689c0596a83d'] = 'BilderlingsPay API реализации';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_4402acab1c8f90dcf4a31dc96833bd86'] = 'Нет валюты для данного модуля';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_82b8bb0d807e6d2e43a068f954c3559f'] = 'Торговый ID, кажется, неправильно';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_73a9b35c6bcc9afe708c4d654e85434e'] = 'Торговый знак, кажется, неправильно';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_37fc03d06648822b3031ed42b3fd53c0'] = 'Торговые слово, кажется, неправильно';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_2cb683130263d10e47d8710245acec5f'] = 'Вы можете найти эти ключи в вашем выразить купец';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_229a7ec501323b94db7ff3157a7623c9'] = 'Торговый ID';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_f81cc37e3b313c82e7cbd38ac1b3c910'] = 'Торговый знак';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_d8df3b22e6de5c2bcf2d825cff0d6fef'] = 'Слово торгового ответ';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_38fb7d24e0d60a048f540ecb18e13376'] = 'Сохранить';
$_MODULE['<{BilderlingsPay}prestashop>BilderlingsPay_dbde44269a7f0459cbb4e5d17a1a32d7'] = 'Оплатить с помощью экспресс торговый';
$_MODULE['<{BilderlingsPay}prestashop>payment_return_d02111c54faf7ef8789e730862b37f9f'] = 'Ваш заказ добавлен';
