<?php
/*
	BilderlingsPay API class
	~~~~~~~~~~~~~~~~~~~~~~~~~~~

	Last update 2010.09.06

*/
 
class BP_Merchant
{
#	var $merchantUrl			= 'https://dev.bilderlingspay.com/process/';
#	var $gatewayUrl				= 'https://dev.bilderlingspay.com/gateway/';
	var $merchantUrl			= 'https://secure-test.bilderlingspay.com/process/';
	var $gatewayUrl				= 'https://secure-test.bilderlingspay.com/gateway/';
#	var $merchantUrl			= 'https://my.bilderlingspay.com/process/';
#	var $gatewayUrl				= 'https://my.bilderlingspay.com/gateway/';
	
	# to change ID/Sign, check http://shop.vintagecandella.eu/waxery/index.php?controller=AdminModules&configure=BilderlingsPay&conf=4
	# my
#	var $merchantId 			= '17-2754972215';
#	var $merchantSign 			= '7fa9305b5e15';

	# DEV and dupe for Secure-TEST
#	var $merchantId 			= '6-1504819546';
#	var $merchantSign 			= '134e99ab8639';

	var $paymentSystems			= 'FD';		// 'EM;WM;MM;RBK;MB;LP;PP;...'
	var $site_answer_word		= 'OK';
	var $site_charset			= 'UTF-8';
	var $site_currency			= 'EUR';	// may be null
	var $site_language			= null;		// 'en'|'ru'|...

	// urls started with '/' will be completed automaticaly by $site_base_url
	var $site_result_url		= null;		// Will be filled automaticaly. eg '/scripts/em_result.php';
	var $site_result_method		= 'POST';
	var $site_success_url		= null;		// Will be filled automaticaly. eg '/scripts/success.php';
	var $site_success_method	= 'POST';
	var $site_cancel_url		= null;		// Will be filled automaticaly. eg '/scripts/cancel.php';
	var $site_cancel_method		= 'POST';
	var $site_base_url			= null;		// Will be filled automaticaly
	var $site_script_self		= null;

	var $transparent			= 0;

	var $on_order_page			= null;
	var $order_page				= null;

	var	$on_payment_success		= null;
	var	$on_payment_cancel		= null;


	var $on_success_page		= null;
	var $success_page			= null;

	var $on_cancel_page			= null;
	var $cancel_page			= null;

	var $params_tr		= array(
	//	'merchant_field'=> 'local_field',
	//	'MERCHANT_ID'	=>'',
	//	'ORDER_NR'		=>'',
	//	'AMOUNT'		=>'',
	//	'CURRENCY'		=>'',
	//	'SYSTEM'		=>'',
	//	'DESC'			=>'',
	//	'INVOICE_ID'	=>'',
	//	'INVOICE_TIME'	=>'',	// "YYYY-MM-DD HH:MM:SS"
	//	'TRANSFER_ID'	=>'',
	//	'TRANSFER_TIME'	=>'',   // "YYYY-MM-DD HH:MM:SS"
	//	'STATUS'		=>''	// 1|0|-1
	);

	//------------------------------------------------------------------------------
	//
	function __construct($params=array())
	{
		foreach($params as $n=>$v) $this->$n = $v;
	}
	//------------------------------------------------------------------------------
	//
	function Run($action=null)
	{
		if (!isset($action)) $action = $this->bydef($_GET, '-a', '');

		switch($action) {
		case '':		$this->Open_Default(); break;
		case 'order':	$this->Open_Order_Page(); break;
		case 'success':	$this->Open_Success_Page(); break;
		case 'cancel':	$this->Open_Cancel_Page(); break;
		case 'result':	$this->HandleResultRequest(); break;

		default:
			if (method_exists($this,$f='Open_'.$action.'_Page')) {
				call_user_func_array(array(&$this, $f), array());
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	function Open_Default()
	{
		$this->HandleResultRequest();
	}
	//------------------------------------------------------------------------------
	//
	function Open_Order_Page($params=null)
	{
		if (!isset($params)) $params = $this->p();

		$args = $this->vget($params, array('order_nr', 'amount', 'currency', 'description', 'info', 'html'), '');

		if (isset($this->on_order_page)) {
			call_user_func_array($this->on_order_page, array_merge(array(&$this), $args));
		} else if (isset($this->order_page)) {
			require($this->order_page);
		} else {
			if (sizeof(array_filter($this->vget($params, array('order_nr', 'amount', 'currency', 'description'))))==4) {
				exit(call_user_func_array(array(&$this, 'GetTransferHtml'), $args));
			}
		}
	}
	//------------------------------------------------------------------------------
	//
	function On_Payment_Success($order_nr, $amount, $currency, $description, $info, $params)
	{
		if (isset($this->on_payment_success)) {
			call_user_func($this->on_payment_success, $order_nr, $amount, $currency, $description, $info, $params);
		}
	}
	//------------------------------------------------------------------------------
	//
	function On_Payment_Cancel($order_nr, $amount, $currency, $description, $info, $params)
	{
		if (isset($this->on_payment_cancel)) {
			call_user_func($this->on_payment_cancel, $order_nr, $amount, $currency, $description, $info, $params);
		}
	}
	//------------------------------------------------------------------------------
	//
	function Open_Success_Page()
	{
		if (isset($this->on_success_page)) {
			call_user_func_array($this->on_success_page, array(&$this));
		} else {
			require(isset($this->success_page) ?$this->success_page :'success.php');
		}
	}
	//------------------------------------------------------------------------------
	//
	function Open_Cancel_Page()
	{
		if (isset($this->on_cancel_page)) {
			call_user_func_array($this->on_cancel_page, array(&$this));
		} else {
			require(isset($this->cancel_page) ?$this->cancel_page :'cancel.php');
		}
	}

	//------------------------------------------------------------------------------
	//
	function GetTransferHtml($order_nr, $amount, $currency, $description, $info=array(), $html='')
	{
		return
			'<html>'
			.'<head><meta http-equiv="Content-Type" content="text/html; '.$this->site_charset.'"></head>'
			.'<body>'
			.$this->GetFormHtml($order_nr, $amount, $currency, $description, $info, $html.'<input type="submit" style="display: none">')
			.'<script>document.forms[0].submit();</script>'
			.'</body>'
			.'</html>';
	}
	//------------------------------------------------------------------------------
	//
	function GetFormHtml($order_nr, $amount, $currency, $description, $info=array(), $html='')
	{
		$fields	= $this->GetFormFields($order_nr, $amount, $currency, $description, $info);
		$form 	= '<form method="POST" action="'.$this->GetFormActionUrl().'" target="_parent">';
		foreach($fields as $n=>$v) {
			$form .= '<input type="hidden" name="'.$n.'" value="'.htmlspecialchars($v).'">';
		}
		$form .= $html.'</form>';

		return $form;
	}
	//------------------------------------------------------------------------------
	//
	function GetFormActionUrl()
	{
		return $this->merchantUrl.'payment';
	}
	//------------------------------------------------------------------------------
	//
	function GetScriptSelf()
	{
		return isset($this->site_script_self) ?$this->site_script_self :$_SERVER['PHP_SELF'];
	}

	//------------------------------------------------------------------------------
	//
	function GetFormFields($order_nr, $amount, $currency, $description, $info=array())
	{
		$amount 	= sprintf('%0.2f',$amount);
		$order_nr	= $order_nr;
		$description= $description;
		$HASH		= md5("$this->merchantId|$order_nr|$amount|$currency|$this->paymentSystems|$this->merchantSign");
        //var_dump("$this->merchantId|$order_nr|$amount|$currency|$this->paymentSystems|$this->merchantSign"); die;
		$fields = array(
			"PAYMENT_MERCHANT_ID"=>$this->merchantId,
			"PAYMENT_ORDER_NR"=>$order_nr,
			"PAYMENT_AMOUNT"=>$amount,
			"PAYMENT_CURRENCY"=>$currency,
			"PAYMENT_SYSTEM"=>$this->paymentSystems,
			"PAYMENT_DESC"=>$description,
			"-lang"=>'ru',
			"PAYMENT_SITE_CHARSET"=>$this->site_charset,
			"PAYMENT_SITE_CURRENCY"=>$this->site_currency,
			"PAYMENT_RESULT_URL"=>$this->SiteUrl($this->site_result_url ?$this->site_result_url : $this->GetScriptSelf()."?-a=result"),
			"PAYMENT_RESULT_METHOD"=>$this->site_result_method,
			"PAYMENT_SUCCESS_URL"=>$this->SiteUrl($this->site_success_url ?$this->site_success_url :$this->GetScriptSelf()."?-a=success"),
			"PAYMENT_SUCCESS_METHOD"=>$this->site_success_method,
			"PAYMENT_CANCEL_URL"=>$this->SiteUrl($this->site_cancel_url ?$this->site_cancel_url :$this->GetScriptSelf()."?-a=cancel"),
			"PAYMENT_CANCEL_METHOD"=>$this->site_cancel_method,
			"PAYMENT_HASH"=>$HASH
		);

		if ($info) foreach($info as $n=>$v) {
			$fields["PAYMENT_INFO_".$n] = $v;
		}
		if ($this->transparent) {
			$fields['PAYMENT_TRANSPARENT'] = 1;
		}
		if (!empty($this->site_language)) {
			$fields['-lang'] = $this->site_language;
		}

		return $fields;
	}
	//------------------------------------------------------------------------------
	//
	function HandleResultRequest()
	{
		$PARAMS =  $this->p();
		// check request
		$REQs = array(
			'MERCHANT_ID', 'ORDER_NR', 'AMOUNT', 'CURRENCY', 'SYSTEM', 'DESC',
			'INVOICE_ID', 'INVOICE_TIME', 'TRANSFER_ID', 'TRANSFER_TIME', 'STATUS','HASH'
		);

		$reqs = $this->p($REQs);
		if (sizeof($reqs)!=sizeof($REQs))  {
			return $this->On_Request_Fail($PARAMS, 'INVALID_REQUEST');
		}
		if ($PARAMS['MERCHANT_ID'] != $this->merchantId) {
			return $this->On_Request_Fail($PARAMS, 'INVALID_MERCHANT');
		}

		if (!$this->IsRequestValid($PARAMS)) {
			//return $this->On_Request_Fail($PARAMS, 'INVALID_HASH');
		}


		// Prepare params
		$info 	= array();
		$params = array();
		foreach($PARAMS as $i=>$v) {
			if (substr($i,0,5)=='INFO_') {
				$info[substr($i,5)] = $v;
			} else {
				$params[isset($this->params_tr[$i]) ?$this->params_tr[$i] :$i] = $v;
			}
		}
        
		// Call handlers
		if ($PARAMS['STATUS']>0) {
			$this->On_Payment_Success(
				$info['cart_id'], $PARAMS['AMOUNT'], $PARAMS['CURRENCY'],
				$PARAMS['DESC'], $info, $params
			);
			exit($this->site_answer_word);
		} else {
			$this->On_Payment_Cancel(
				$info['cart_id'], $PARAMS['AMOUNT'], $PARAMS['CURRENCY'],
				$PARAMS['DESC'], $info, $params
			);
			exit($this->site_answer_word);
		}
	}
	//------------------------------------------------------------------------------
	//
	function IsRequestValid($PARAMS=null)
	{
		if (!isset($PARAMS)) $PARAMS = $this->p();

    	/*$hash = join('|',$this->vget($PARAMS,array(
    		'MERCHANT_ID','ORDER_NR','AMOUNT','CURRENCY','SYSTEM','INVOICE_ID','TRANSFER_ID'
    	)));
    	$hash.= '|'.$this->merchantSign;*/
        
        //$hash = "{$PARAMS['MERCHANT_ID']}|{$PARAMS['ORDER_NR']}|{$PARAMS['AMOUNT']}|{$PARAMS['CURRENCY']}|{$this->paymentSystems}|{$PARAMS['INVOICE_ID']}|{$PARAMS['TRANSFER_ID']}|{$this->merchantSign}";
        
        $hash = md5("{$PARAMS['MERCHANT_ID']}|{$PARAMS['INFO_order']}|{$PARAMS['AMOUNT']}|{$PARAMS['CURRENCY']}|{$this->paymentSystems}|{$this->merchantSign}");
        //var_dump($hash, $PARAMS['HASH'], "{$PARAMS['MERCHANT_ID']}|{$PARAMS['INFO_order']}|{$PARAMS['AMOUNT']}|{$PARAMS['CURRENCY']}|{$this->paymentSystems}|{$this->merchantSign}");die;
        
		return $hash == $PARAMS['HASH'];
	}
	//------------------------------------------------------------------------------
	//
	function On_Request_Fail($params, $code) {
		exit('FAIL: '.$code);
	}

	//------------------------------------------------------------------------------
	//
	function SiteUrl($url) {

		return ($url && $url[0]=='/') ?$this->SiteBaseUrl().$url :$url;
	}
	//------------------------------------------------------------------------------
	//
	function SiteBaseUrl() {
		if (empty($this->site_base_url)) {
			$http = (empty($_SERVER['HTTPS']) || strtolower($_SERVER['HTTPS'])=='off') ?'http' :'https';
			$this->site_base_url = $http.'://'.$this->bydef($_SERVER, 'HTTP_HOST', $this->bydef($_SERVER, 'SERVER_NAME'));
		}
		return $this->site_base_url;
	}
	//------------------------------------------------------------------------------
	//
	function p($id=null, $def=null)
	{
		if (!isset($this->p)) {
			$this->p =  !empty($_POST) ?$_POST :$_GET;

			if (ini_get('magic_quotes_gpc')) {
				array_walk_recursive($this->p, create_function('&$v,$n','$v=stripslashes($v);'));
			}
		}
		return isset($id) ?$this->vget($this->p, $id, $def) :$this->p;
	}
	//------------------------------------------------------------------------------
	//
	function bydef($a, $id, $def=null) {
		return isset($a[$id]) ?$a[$id] :$def;
	}
	//------------------------------------------------------------------------------
	//
	function vget($vars, $ids, $def=null)
	{
		if (!isset($ids)) return $vars;
		if (is_array($ids)) {
			if (!isset($vars)) return null;
			$res = array();
			foreach($ids as $as=>$id) {
				if (is_scalar($id)) {
					if (isset($vars[$id]) || array_key_exists($id,$vars))
						$res[is_int($as)?$id:$as] = $vars[$id];
					else
					if (isset($def))
						$res[is_int($as)?$id:$as] = $def;
				} else
				if (is_array($id)) {
					$res[$as] = vget($vars, $id, $def);
				}
			}
			return $res;
		}
		return isset($vars[$ids]) ?$vars[$ids] :$def;
	}

	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	//
	//
	function request_PaymentHistory($where)
	{
		$request = array(
			'MERCHANT_ID'=>$this->merchantId
		);
		$request += $this->vget($where, array(
			'WHERE_TIME_FROM',
			'WHERE_TIME_TILL',
			'WHERE_STATUS',
			'WHERE_ORDER_NR',
			'WHERE_INV_ID',
			'WHERE_INV_AMOUNT_FROM',
			'WHERE_INV_AMOUNT_TILL',
			'WHERE_INV_CURRENCY',
			'WHERE_PAY_AMOUNT_FROM',
			'WHERE_PAY_AMOUNT_TILL',
			'WHERE_PAY_CURRENCY',
			'RECORD_FROM',
			'RECORD_COUNT',
			'RECORD_FIELDS'
		));

		$request['HASH'] = md5(join('|',$request).'|'.$this->merchantSign);
		if (!empty($this->site_charset)) {
			$request['SITE_CHARSET'] = $this->site_charset;
		}

		return $this->httpRequest($this->gatewayUrl.'paymenthistory', $request);
	}
	//------------------------------------------------------------------------------
	//
	function request_UserInfo($params)
	{
		$request = array(
			'MERCHANT_ID'=>$this->merchantId
		);
		$request += $this->vget($params, array(
			'EMAIL',
		));

		if (sizeof(array_filter($request))<2) return false;

		$request['HASH'] = md5(join('|',$request).'|'.$this->merchantSign);
		if (!empty($this->site_charset)) {
			$request['SITE_CHARSET'] = $this->site_charset;
		}

		return $this->httpRequest($this->gatewayUrl.'userinfo', $request);
	}
	//------------------------------------------------------------------------------
	//
	function request_BalanceInfo($params)
	{
		$request = array(
			'MERCHANT_ID'=>$this->merchantId,
		);
		$request += $this->vget($params, array(
			'ECURRENCY',	// EMZ;EMR;EME;...
			'CURRENCY',		// USD;RUR;EUR;...
		), '');

		if (sizeof(array_filter($request))<2) return false;

		$request['HASH'] = md5(join('|',$request).'|'.$this->merchantSign);

		return $this->httpRequest($this->gatewayUrl.'balanceinfo', $request);
	}
	//------------------------------------------------------------------------------
	//
	function request_PayoutMake($params)
	{
		$request = array(
			'MERCHANT_ID'=>$this->merchantId
		);
		$request += $this->vget($params, array(
			'PAYOUT_ORDER_NR',
			'PAYOUT_AMOUNT',
			'PAYOUT_CURRENCY',
			'PAYOUT_DESC',
			'PAYOUT_PAYEE_EMAIL',
			'PAYOUT_PAYEE_CREATE'
		));


		if (sizeof(array_filter($request))<6) return false;

		$request['HASH'] = md5(join('|',$this->vget($request,array('MERCHANT_ID','PAYOUT_ORDER_NR','PAYOUT_AMOUNT','PAYOUT_CURRENCY','PAYOUT_PAYEE_EMAIL'))).'|'.$this->merchantSign);
		if (!empty($this->site_charset)) {
			$request['SITE_CHARSET'] = $this->site_charset;
		}

		return $this->httpRequest($this->gatewayUrl.'payout_make', $request);
	}
	//------------------------------------------------------------------------------
	//
	function request_PayoutCheck($params)
	{
		$request = array(
			'MERCHANT_ID'=>$this->merchantId
		);
		$request += $this->vget($params, array(
			'PAYOUT_ID',
			'PAYOUT_ORDER_NR'
		));

		if (sizeof(array_filter($request))<2) return false;

		$request['HASH'] = md5(join('|',$request).'|'.$this->merchantSign);
		if (!empty($this->site_charset)) {
			$request['SITE_CHARSET'] = $this->site_charset;
		}

		return $this->httpRequest($this->gatewayUrl.'payout_check', $request);
	}

	//------------------------------------------------------------------------------
	//
	function httpRequest($url, $params, $method='POST')
	{
	//	print "[<pre>".print_r($params,true)."</pre>]";
		set_time_limit(0);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);

		if (strtoupper($method)=='GET') {
			curl_setopt($ch, CURLOPT_HTTPGET, 1);
			curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($params));
		} else {
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		}


		$contents = curl_exec($ch);
		$this->last_httpRequest_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if (!$contents) {
			$this->last_httpRequest_errno = curl_errno($ch);
			$this->last_httpRequest_error = curl_error($ch);
		}

		curl_close($ch);

//		print "[<pre>".print_r($contents,true)."</pre>]";

		if ($contents) {
			if (substr($contents, 0, 7)!='STATUS=') {
				return array('STATUS'=>'FAIL', 'CODE'=>'HTTP_INVALID_CONTENT', 'HTTP_CONTENT'=>$contents);
			}
			return $this->ParseContent($contents);
		}

	 	return array(
	 		'STATUS'=>'FAIL',
	 		'CODE'=>'HTTP_ERROR',
	 		'HTTP_CODE' =>$this->last_httpRequest_code,
	 		'HTTP_ERRNO'=>$this->last_httpRequest_errno,
	 		'HTTP_ERROR'=>$this->last_httpRequest_error
	 	);
	}
	//------------------------------------------------------------------------------
	//
	function ParseParams($params)
	{
		$result = array();
		if (!$params)  return $result;

		foreach(explode(';', $params) as $pair) {
			$pair = explode("=", $pair, 2);
			$result[$pair[0]] = urldecode($this->bydef($pair,1));
		}
		return $result;
	}
	//------------------------------------------------------------------------------
	//
	function ParseContent(&$content)
	{
		$res = array();
		for($c=explode("\n",$content,2); strlen($c[0]); $c=explode("\n",$this->bydef($c,1),2))
		{
			if ($c[0]{0}=='=') {
				$item = explode(':', substr($c[0], 1));
				if (!empty($c[1])) {
					switch($this->bydef($item,1)) {
					case 'RECS':	$res[$item[0]] = $this->ParseContentRecs($c[1]); break;
					default: 		$res[$item[0]] = $this->ParseContent($c[1]); break;
					}
				}
			} else {
				$res += $this->ParseParams($c[0]);
			}
		}
		$content = $this->bydef($c,1);

		return $res;
	}
	//------------------------------------------------------------------------------
	//
	function ParseContentRecs(&$content)
	{
		$recs = array();
		$c = explode("\n", $content, 2);
		$fields = explode(';', $c[0]);

		$c = $this->bydef($c,1);
		for($c=explode("\n",$c,2); strlen($c[0]); $c=explode("\n",$this->bydef($c,1),2))
		{
			$values = array_map('urldecode', explode(';',$c[0]));
			$recs[] = array_combine($fields, $values);
		}
		$content = $this->bydef($c,1);
		return $recs;
	}
}
