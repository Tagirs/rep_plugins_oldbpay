<?php

/*

* 2007-2011 PrestaShop

*

* NOTICE OF LICENSE

*

*  @author PrestaShop SA <contact@prestashop.com>

*  @copyright  2007-2011 PrestaShop SA

*  @version  Release: $Revision: 1.4 $

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Registered Trademark & Property of PrestaShop SA

*/



include(dirname(__FILE__).'/../../config/config.inc.php');



$controller = new FrontController();

$controller->init();



include(dirname(__FILE__).'/BilderlingsPay.php');



$BilderlingsPay = new BilderlingsPay();

if ($cart->id_customer == 0 OR $cart->id_address_delivery == 0 OR $cart->id_address_invoice == 0 OR !$BilderlingsPay->active)

	Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');



$customer = new Customer((int)$cart->id_customer);



if (!Validate::isLoadedObject($customer))

	Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');



$customer = new Customer((int)($cart->id_customer));

$total = $cart->getOrderTotal(true, Cart::BOTH);

//$BilderlingsPay->validateOrder((int)($cart->id), _PS_OS_PREPARATION_, $total, $BilderlingsPay->displayName, NULL, array(), NULL, false,$customer->secure_key);

$order = new Order((int)($BilderlingsPay->currentOrder));

//var_dump($cart); die;

echo $BilderlingsPay->pay($cart);