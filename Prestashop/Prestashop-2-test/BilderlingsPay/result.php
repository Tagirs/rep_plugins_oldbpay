<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/prestoshop-BilderlingsPay.inc.php');
include(dirname(__FILE__).'/BilderlingsPay.php');

//global $BP_request;
global $BilderlingsPay;

//$input = mb_convert_encoding(urldecode($_REQUEST['INFO_cart_id']), 'UTF-8', mb_detect_encoding($_REQUEST['INFO_cart_id']));
//$BP_request = strip_tags(htmlentities($input, ENT_QUOTES, 'UTF-8'));
//var_dump($BP_request);die;

$BilderlingsPay = new BilderlingsPay();
$currency = $BilderlingsPay->getCurrency();

$merchant = new Prestopshop_BP_Merchant($currency->iso_code);
$merchant->on_payment_success	= 'BilderlingsPay_payment_success';
$merchant->on_payment_cancel	= 'BilderlingsPay_payment_cancel';
$merchant->HandleResultRequest();

function BilderlingsPay_payment_success($order_nr, $amount, $currency)
{   
    global $BilderlingsPay;
	$BilderlingsPay->validateOrder($order_nr, _PS_OS_PAYMENT_, $amount, $BilderlingsPay->displayName);
}

function BilderlingsPay_payment_cancel($order_nr, $amount, $currency)
{
	global $BilderlingsPay;
	$BilderlingsPay->validateOrder($order_nr, _PS_OS_ERROR_, 0, $BilderlingsPay->displayName);
}