<?php

require_once('BilderlingsPay.inc.php');

class Prestopshop_BP_Merchant extends BP_Merchant {

	public function Prestopshop_BP_Merchant($currency)
	{

		global $cookie;
		$lang = Language::getLanguage($cookie->id_lang);

		$this->merchantId			= Configuration::get('BilderlingsPay_ID');
		$this->merchantSign			= Configuration::get('BilderlingsPay_SIGN');
		$this->site_currency		= $currency;
		$this->site_answer_word		= Configuration::get('BilderlingsPay_WORD');
		$this->site_language		= $lang['iso_code'];

		$this->site_result_url = Tools::getHttpHost(true, true).__PS_BASE_URI__.'modules/BilderlingsPay/result.php';
		$this->site_success_url = $this->site_cancel_url = Tools::getHttpHost(true, true).__PS_BASE_URI__;
	}

}
