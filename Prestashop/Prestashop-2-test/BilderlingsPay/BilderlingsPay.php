<?php

class BilderlingsPay extends PaymentModule
{
    function __construct()
    {
        $this->name = 'BilderlingsPay';
        $this->tab = 'payments_gateways';
        $this->version = 1.0;

		$this->currencies = true;
		$this->currencies_mode = 'radio';

        parent::__construct();

        $this->displayName = $this->l('BilderlingsPay');
        $this->description = $this->l('BilderlingsPay API implementation');
		
		if (!sizeof(Currency::checkPaymentCurrencies($this->id)))
			$this->warning = $this->l('No currency set for this module');
    }

	function install()
	{
		if (!parent::install()
			OR !$this->registerHook('payment')
			OR !$this->registerHook('paymentReturn')
			OR !Configuration::updateValue('BilderlingsPay_ID', '')
			OR !Configuration::updateValue('BilderlingsPay_SIGN', '')
			OR !Configuration::updateValue('BilderlingsPay_WORD', 'OK'))
			return false;
		return true;
	}

	function uninstall()
	{
		return (
			parent::uninstall() AND
			Configuration::deleteByName('BilderlingsPay_ID') AND
			Configuration::deleteByName('BilderlingsPay_SIGN') AND
			Configuration::deleteByName('BilderlingsPay_WORD')
		);
    }
	
	function getContent()
	{
		global $currentIndex, $cookie;
		
		if (Tools::isSubmit('submitBilderlingsPay'))
		{
			$errors = array();
			if (($merchant_id = Tools::getValue('BilderlingsPay_id')) AND preg_match('/[0-9-]+/', $merchant_id))
				Configuration::updateValue('BilderlingsPay_ID', $merchant_id);
			else
				$errors[] = '<div class="warning warn"><h3>'.$this->l('Merchant ID seems to be wrong').'</h3></div>';

			if (($merchant_key = Tools::getValue('BilderlingsPay_sign')) AND preg_match('/[a-zA-Z0-9_-]+/', $merchant_key))
				Configuration::updateValue('BilderlingsPay_SIGN', $merchant_key);
			else
				$errors[] = '<div class="warning warn"><h3>'.$this->l('Merchant sign seems to be wrong').'</h3></div>';

			if (($merchant_word = Tools::getValue('BilderlingsPay_word')) AND preg_match('/[a-zA-Z0-9_-]+/', $merchant_word))
				Configuration::updateValue('BilderlingsPay_WORD', $merchant_word);
			else
				$errors[] = '<div class="warning warn"><h3>'.$this->l('Merchant word seems to be wrong').'</h3></div>';

			if (!sizeof($errors))
				Tools::redirectAdmin($currentIndex.'&configure=BilderlingsPay&token='.Tools::getValue('token').'&conf=4');
			foreach ($errors as $error)
				echo $error;
		}
		
		$html = '<h2>'.$this->displayName.'</h2>
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset>
			<legend><img src="'.__PS_BASE_URI__.'modules/BilderlingsPay/logo.gif" />'.$this->l('Settings').'</legend>
				<p>'.$this->l('You can find these keys in your BilderlingsPay').'</p>
				<label>
					'.$this->l('Merchant ID').'
				</label>
				<div class="margin-form">
					<input type="text" name="BilderlingsPay_id" value="'.Tools::getValue('BilderlingsPay_id', Configuration::get('BilderlingsPay_ID')).'" />
				</div>

				<label>
					'.$this->l('Merchant sign').'
				</label>
				<div class="margin-form">
					<input type="text" name="BilderlingsPay_sign" value="'.Tools::getValue('BilderlingsPay_sign', Configuration::get('BilderlingsPay_SIGN')).'" />
				</div>

				<label>
					'.$this->l('Merchant answer word').'
				</label>
				<div class="margin-form">
					<input type="text" name="BilderlingsPay_word" value="'.Tools::getValue('BilderlingsPay_word', Configuration::get('BilderlingsPay_WORD')).'" />
				</div>

				<div class="clear center"><input type="submit" name="submitBilderlingsPay" class="button" value="'.$this->l('   Save   ').'" /></div>
			</fieldset>
		</form>';

		return $html;
	}

	function hookPayment($params)
	{
		if (!$this->active)
			return ;

		global $smarty;

		$smarty->assign(array(
			'this_path' => $this->_path,
			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
		));

		return $this->display(__FILE__, 'pay.tpl');
	}

	function hookPaymentReturn($params)
	{
		if (!$this->active)
			return ;

		return $this->display(__FILE__, 'payment_return.tpl');
	}

	function validateOrder($id_cart, $id_order_state, $amountPaid, $paymentMethod = 'Unknown', $message = NULL, $extraVars = array(), $currency_special = NULL, $dont_touch_amount = false, $secure_key = false, Shop $shop = NULL)
	{
		if (!$this->active)
			return ;

		parent::validateOrder($id_cart, $id_order_state, $amountPaid, $paymentMethod, $message, $extraVars);
	}

	function pay($cart)
	{
		require_once('prestoshop-BilderlingsPay.inc.php');

		//$currency = $this->getCurrency();
        $cur = new Currency((int)($cart->id_currency));

		$total_cost = $cart->getOrderTotal();
		$merchant = new Prestopshop_BP_Merchant($cur->iso_code);
        
        //var_dump($cart); die;
        $order_nr = time();
		return $merchant->GetTransferHtml($order_nr, $total_cost, $cur->iso_code, '', array('cart_id' => $cart->id, 'order' => $order_nr));
	}
}

