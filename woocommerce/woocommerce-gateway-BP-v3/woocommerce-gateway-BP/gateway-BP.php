<?php
/**
 * Plugin Name: WooCommerce BilderlingsPay
  * Plugin URI: https://www.bilderlingspay.com/
 * Description: BilderlingsPay Gateway Plugin for WooCommerce.
 * Version: 1.0.0
 * Author: BilderlingsPay
 * Author URI: https://www.bilderlingspay.com/
 * Developer: BilderlingsPay
 * Developer URI: https://www.bilderlingspay.com/
  * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */



add_action('plugins_loaded', 'woocommerce_BP_init');
function woocommerce_BP_init()
{
	if (!class_exists('WC_Payment_Gateway')) {
		return;
	}
	require_once(plugin_basename('classes/class-wc-gateway-BP.php'));

}


function woocommerce_BP_add_gateway($methods)
{
	$methods[] = 'WC_Gateway_BP';
	return $methods;
}


add_filter('woocommerce_payment_gateways', 'woocommerce_BP_add_gateway');