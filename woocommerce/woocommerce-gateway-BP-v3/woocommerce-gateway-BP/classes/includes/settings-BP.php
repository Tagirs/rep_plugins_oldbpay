<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

return array(
	'enabled' => array(
		'title'   => __( 'Enable/Disable', 'woocommerce' ),
		'type'    => 'checkbox',
		'label'   => __( 'Enable BilderlingsPay standard', 'woocommerce' ),
		'default' => 'yes'
	),
	'title' => array(
		'title'       => __( 'Title', 'woocommerce' ),
		'type'        => 'text',
		'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
		'default'     => __( 'BilderlingsPay', 'woocommerce' ),
		'desc_tip'    => true,
	),
	'description' => array(
		'title'       => __( 'Description', 'woocommerce' ),
		'type'        => 'text',
		'desc_tip'    => true,
		'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ),
		'default'     => __( 'Pay via Visa, MasterCard or Maestro card', 'woocommerce' )
	),
	'email' => array(
		'title'       => __( 'Merchant contact e-mail', 'woocommerce' ),
		'type'        => 'email',
		'description' => __( 'Please enter your customer support email address', 'woocommerce' ),
		'default'     => get_option( 'admin_email' ),
		'desc_tip'    => true,
		'placeholder' => 'you@youremail.com'
	),
	'testmode' => array(
		'title'       => __( 'Test mode', 'woocommerce' ),
		'type'        => 'checkbox',
		'label'       => __( 'Test mode for BilderlingsPay', 'woocommerce' ),
		'default'     => 'no',
		'description' => 'Ask customer manager for BilderlingsPay Test server access',
	),
	'advanced' => array(
		'title'       => __( 'Advanced options', 'woocommerce' ),
		'type'        => 'title',
		'description' => '',
	),
	
    'mid' => array(
		'title'       => __( 'Merchant ID', 'woocommerce' ),
		'type'        => 'text',
		'description' => __( 'Merchant ID for BilderlingsPay', 'woocommerce' ),
		'default'     => '',
		'desc_tip'    => true,
		'placeholder' => ''
	),
	'msign' => array(
		'title'       => __( 'Merchant Signature', 'woocommerce' ),
		'type'        => 'text',
		'description' => __( 'Merchant Signature for BilderlingsPay', 'woocommerce' ),
		'default'     => '',
		'desc_tip'    => true,
	)
);
