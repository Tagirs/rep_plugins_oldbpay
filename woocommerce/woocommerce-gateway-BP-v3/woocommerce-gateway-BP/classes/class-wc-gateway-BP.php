<?php

class WC_Gateway_BP extends WC_Payment_Gateway {
	public static $log_enabled = false;

	public static $log;

	protected $gateway;
	protected $notify_url;

	public function __construct()
	{
		$this->gateway = $this;
		$this->notify_url = WC()->api_request_url('WC_Gateway_BP');

$this->id                 	= 'bp'; 
		    	$this->method_title = __('Bilderlings Pay', 'woocommerce');
		$this->method_description = sprintf(__('BilderlingsPay plugin does not have additional dependencies. Check the %ssystem status%s page to check basic requirements and system health.', 'woocommerce'), '<a href="' . admin_url('admin.php?page=wc-status') . '">', '</a>');
		$this->supports = array(
			'products'
		);


		$this->has_fields = false;
		$this->order_button_text = __('Proceed to BilderlingsPay', 'woocommerce');
$this->init_form_fields();
		$this->init_settings();


		$this->title = $this->get_option('title');
		$this->description = $this->get_option('description');
		$this->testmode = 'yes' === $this->get_option('testmode', 'no');

		$this->mid = $this->get_option('mid');
		$this->msign = $this->get_option('msign');
		$this->deflang = $this->get_option('deflang');

add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
		$posted = wp_unslash($_POST);
		if (!$this->is_valid_for_use()) {
			$this->enabled = 'no';
		} else {
			$order_id = $posted['INFO_ORDER_ID'];
			$status = $posted['STATUS'];
			$order = wc_get_order($order_id);

			if ($posted['STATUS']) {
				$hash = md5("$this->mid|{$_POST['ORDER_NR']}|{$_POST['AMOUNT']}|{$_POST['CURRENCY']}|{$_POST['SYSTEM']}|$this->msign");

				if ($hash != $_POST['HASH']) $this->payment_status_failed($order, $posted);

				if ($status == '1') $this->payment_status_completed($order, $posted);
				else if ($status == '-1') $this->payment_status_failed($order, $posted);
				else $this->payment_status_failed($order, $posted);
			}
		}

                

	  
	} //function ends

function payment_status_completed($order, $posted)
	{
		if ($order->has_status('completed')) {
			exit;
		}

		if ('1' === $posted['STATUS']) {
			// WC_Gateway_BP::log('Order #' . $order->id . ' completed.');
			$this->payment_complete($order, (!empty($posted['ORDER_NR']) ? wc_clean($posted['ORDER_NR']) : ''), __('Payment completed', 'woocommerce'));
		} else {
			$this->payment_on_hold($order, sprintf(__('Payment pending: %s', 'woocommerce'), $posted['ORDER_NR']));
		}
	}

	function payment_status_failed($order, $posted)
	{
		$order->update_status('failed', sprintf(__('Payment %s', 'woocommerce'), wc_clean($posted['ORDER_NR'])));
	}

	protected function payment_complete($order, $txn_id = '', $note = '')
	{
		$order->add_order_note($note);
		$order->payment_complete($txn_id);
	}

	protected function payment_on_hold($order, $reason = '')
	{
		$order->update_status('on-hold', $reason);
		$order->reduce_order_stock();
		WC()->cart->empty_cart();
	}

	public static function log($message)
	{
		if (self::$log_enabled) {
			if (empty(self::$log)) {
				self::$log = new WC_Logger();
			}
			self::$log->add('bilderlingspay', $message);
		}
	}

	public function get_icon()
	{
		$icon_html = '';
		return apply_filters('woocommerce_gateway_icon', $icon_html, $this->id);
	}

	public function is_valid_for_use()
	{
		return in_array(get_woocommerce_currency(), array('EUR', 'USD', 'GBP', 'AUD', 'RUB', 'UAH'));
	}

	public function admin_options()
	{
		if ($this->is_valid_for_use()) {
			parent::admin_options();
		} else {
			?>
			<div class="inline error"><p><strong><?php _e('Gateway Disabled', 'woocommerce'); ?></strong>: <?php _e('BilderlingsPay does not support your store currency.', 'woocommerce'); ?></p></div>
			<?php
		}
	}

	public function init_form_fields()
	{
		$this->form_fields = include('includes/settings-BP.php');
	}

public function process_payment($order_id)
	{
		$order = wc_get_order($order_id);

		$order_nr = time() . '-' . $order_id;
		$amount = get_post_meta($order->id, '_order_total', true);
		$currency = get_woocommerce_currency();
		$payment_system = 'FD';

		$hash = md5("$this->mid|$order_nr|$amount|$currency|$payment_system|$this->msign");

		$lang = get_locale();
		if (strlen($lang) > 2) {
			$lang = substr($lang, 0, 2);
		} else {
			$lang = 'ru';
		}

		$params = array(
			'-lang' => $lang,
			'PAYMENT_MERCHANT_ID' => $this->mid,
			'PAYMENT_ORDER_NR' => $order_nr,
			'PAYMENT_AMOUNT' => $amount,
			'PAYMENT_CURRENCY' => $currency,
			'PAYMENT_SYSTEM' => $payment_system,
			'PAYMENT_DESC' => $order->post->post_title,
			'PAYMENT_SITE_CHARSET' => get_bloginfo('charset'),
			'PAYMENT_SITE_CURRENCY' => $currency,
			'PAYMENT_RESULT_URL' => $this->notify_url,
			'PAYMENT_RESULT_METHOD' => 'POST',
			'PAYMENT_SUCCESS_URL' => $this->gateway->get_return_url($order),
			'PAYMENT_SUCCESS_METHOD' => 'POST',
			'PAYMENT_CANCEL_URL' => $order->get_cancel_order_url_raw(),
			'PAYMENT_CANCEL_METHOD' => 'POST',
			'PAYMENT_CUSTOMER_EMAIL' => $order->billing_email,
			'PAYMENT_HASH' => $hash,
			'PAYMENT_CUSTOMER_PHONE' => '',
			'PAYMENT_CUSTOMER_NAME' => $order->billing_first_name,
			'PAYMENT_CUSTOMER_FNAME' => $order->billing_first_name,
			'PAYMENT_CUSTOMER_LNAME' => $order->billing_last_name,
			'PAYMENT_CUSTOMER_CITY' => $order->billing_city,
			'PAYMENT_CUSTOMER_COUNTRY' => $order->billing_country,
			'PAYMENT_INFO_ORDER_ID' => $order_id

		);

		$args = http_build_query($params);
		$url = $this->testmode ? 'secure-test' : 'my';

		return array(
			'result' => 'success',
			'redirect' => 'https://' . $url . '.bilderlingspay.com/process/payment?' . $args
		);
	}

	public function can_refund_order($order)
	{
		return false;
	}




} //class ends
