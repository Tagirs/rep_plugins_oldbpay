<?php
// Heading
$_['heading_title']					 = 'Bilderlings Pay';

// Text
$_['text_payment']					 = 'Payment';
$_['text_success']					 = 'Success: You have modified Bilderlings Pay account details!';
$_['text_edit']                      = 'Edit Bilderlings Pay';
$_['text_bildpay']				 = '<a target="_BLANK" href="https://www.bilderlingspay.com/"><img src="view/image/payment/bildpay.png" alt="Bilderlings Pay" title="Bilderlings Pay" style="border: 1px solid #EEEEEE;" /></a>';


// Entry
$_['entry_merchant_id']				 = 'Merchant ID';
$_['entry_private_key']				 = 'Private Key';
$_['entry_test']					 = 'Sandbox Mode';
$_['entry_debug']					 = 'Debug Mode';
$_['entry_total']					 = 'Total';
$_['entry_canceled_reversal_status'] = 'Canceled Reversal Status';
$_['entry_completed_status']		 = 'Completed Status';
$_['badsign_status']         = 'Bad Signature Status';
/*$_['entry_denied_status']			 = 'Denied Status';
$_['entry_expired_status']			 = 'Expired Status';
$_['entry_failed_status']			 = 'Failed Status';
$_['entry_pending_status']			 = 'Pending Status';
$_['entry_processed_status']		 = 'Processed Status';
$_['entry_refunded_status']			 = 'Refunded Status';
$_['entry_reversed_status']			 = 'Reversed Status';
$_['entry_voided_status']			 = 'Voided Status';*/
$_['entry_geo_zone']				 = 'Geo Zone';
$_['entry_status']					 = 'Status';
$_['entry_sort_order']				 = 'Sort Order';
$_['badsign']                                                                = 'Bad signature';

// Tab
$_['tab_general']					 = 'General';
$_['tab_order_status']       		 = 'Order Status';

// Help
$_['help_test']						 = 'Use the live or testing (sandbox) gateway server to process transactions?';
$_['help_debug']			    	 = 'Logs additional information to the system log';
$_['help_total']					 = 'The checkout total the order must reach before this payment method becomes active';

// Error
$_['error_permission']				 = 'Warning: You do not have permission to modify payment Bilderlings Pay!';
$_['error_merchant_id']				 = 'Merchant ID required!';
$_['error_private_key']				 = 'Private Key required!';