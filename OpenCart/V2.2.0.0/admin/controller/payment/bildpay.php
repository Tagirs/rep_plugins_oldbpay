<?php
class ControllerPaymentBildPay extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/bildpay');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('bildpay', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token']. '&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');

		$data['entry_merchant_id'] = $this->language->get('entry_merchant_id');
		$data['entry_private_key'] = $this->language->get('entry_private_key');
		$data['entry_test'] = $this->language->get('entry_test');
		$data['entry_debug'] = $this->language->get('entry_debug');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_canceled_reversal_status'] = $this->language->get('entry_canceled_reversal_status');
		$data['entry_completed_status'] = $this->language->get('entry_completed_status');
		$data['badsign_status'] = $this->language->get('badsign_status');
		/*$data['entry_denied_status'] = $this->language->get('entry_denied_status');
		$data['entry_expired_status'] = $this->language->get('entry_expired_status');
		$data['entry_failed_status'] = $this->language->get('entry_failed_status');
		$data['entry_pending_status'] = $this->language->get('entry_pending_status');
		$data['entry_processed_status'] = $this->language->get('entry_processed_status');
		$data['entry_refunded_status'] = $this->language->get('entry_refunded_status');
		$data['entry_reversed_status'] = $this->language->get('entry_reversed_status');
		$data['entry_voided_status'] = $this->language->get('entry_voided_status');*/
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['help_test'] = $this->language->get('help_test');
		$data['help_debug'] = $this->language->get('help_debug');
		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_order_status'] = $this->language->get('tab_order_status');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['merchant_id'])) {
			$data['error_merchant_id'] = $this->error['merchant_id'];
		} else {
			$data['error_merchant_id'] = '';
		}

		if (isset($this->error['private_key'])) {
			$data['error_private_key'] = $this->error['private_key'];
		} else {
			$data['error_private_key'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payment'),
			'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/bildpay', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('payment/bildpay', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], true);

		if (isset($this->request->post['bildpay_merchant_id'])) {
			$data['bildpay_merchant_id'] = $this->request->post['bildpay_merchant_id'];
		} else {
			$data['bildpay_merchant_id'] = $this->config->get('bildpay_merchant_id');
		}

		if (isset($this->request->post['bildpay_private_key'])) {
			$data['bildpay_private_key'] = $this->request->post['bildpay_private_key'];
		} else {
			$data['bildpay_private_key'] = $this->config->get('bildpay_private_key');
		}

		if (isset($this->request->post['bildpay_test'])) {
			$data['bildpay_test'] = $this->request->post['bildpay_test'];
		} else {
			$data['bildpay_test'] = $this->config->get('bildpay_test');
		}


		if (isset($this->request->post['bildpay_debug'])) {
			$data['bildpay_debug'] = $this->request->post['bildpay_debug'];
		} else {
			$data['bildpay_debug'] = $this->config->get('bildpay_debug');
		}

		if (isset($this->request->post['bildpay_total'])) {
			$data['bildpay_total'] = $this->request->post['bildpay_total'];
		} else {
			$data['bildpay_total'] = $this->config->get('bildpay_total');
		}

		if (isset($this->request->post['bildpay_canceled_reversal_status_id'])) {
			$data['bildpay_canceled_reversal_status_id'] = $this->request->post['bildpay_canceled_reversal_status_id'];
		} else {
			$data['bildpay_canceled_reversal_status_id'] = $this->config->get('bildpay_canceled_reversal_status_id');
		}

		if (isset($this->request->post['bildpay_completed_status_id'])) {
			$data['bildpay_completed_status_id'] = $this->request->post['bildpay_completed_status_id'];
		} else {
			$data['bildpay_completed_status_id'] = $this->config->get('bildpay_completed_status_id');
		}

		if (isset($this->request->post['bildpay_badsign_status_id'])) {
			$data['bildpay_badsign_status_id'] = $this->request->post['bildpay_badsign_status_id'];
		} else {
			$data['bildpay_badsign_status_id'] = $this->config->get('bildpay_badsign_status_id');
		}

		/*if (isset($this->request->post['bildpay_denied_status_id'])) {
			$data['bildpay_denied_status_id'] = $this->request->post['bildpay_denied_status_id'];
		} else {
			$data['bildpay_denied_status_id'] = $this->config->get('bildpay_denied_status_id');
		}

		if (isset($this->request->post['bildpay_expired_status_id'])) {
			$data['bildpay_expired_status_id'] = $this->request->post['bildpay_expired_status_id'];
		} else {
			$data['bildpay_expired_status_id'] = $this->config->get('bildpay_expired_status_id');
		}

		if (isset($this->request->post['bildpay_failed_status_id'])) {
			$data['bildpay_failed_status_id'] = $this->request->post['bildpay_failed_status_id'];
		} else {
			$data['bildpay_failed_status_id'] = $this->config->get('bildpay_failed_status_id');
		}

		if (isset($this->request->post['bildpay_pending_status_id'])) {
			$data['bildpay_pending_status_id'] = $this->request->post['bildpay_pending_status_id'];
		} else {
			$data['bildpay_pending_status_id'] = $this->config->get('bildpay_pending_status_id');
		}

		if (isset($this->request->post['bildpay_processed_status_id'])) {
			$data['bildpay_processed_status_id'] = $this->request->post['bildpay_processed_status_id'];
		} else {
			$data['bildpay_processed_status_id'] = $this->config->get('bildpay_processed_status_id');
		}

		if (isset($this->request->post['bildpay_refunded_status_id'])) {
			$data['bildpay_refunded_status_id'] = $this->request->post['bildpay_refunded_status_id'];
		} else {
			$data['bildpay_refunded_status_id'] = $this->config->get('bildpay_refunded_status_id');
		}

		if (isset($this->request->post['bildpay_reversed_status_id'])) {
			$data['bildpay_reversed_status_id'] = $this->request->post['bildpay_reversed_status_id'];
		} else {
			$data['bildpay_reversed_status_id'] = $this->config->get('bildpay_reversed_status_id');
		}

		if (isset($this->request->post['bildpay_voided_status_id'])) {
			$data['bildpay_voided_status_id'] = $this->request->post['bildpay_voided_status_id'];
		} else {
			$data['bildpay_voided_status_id'] = $this->config->get('bildpay_voided_status_id');
		}*/

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['bildpay_geo_zone_id'])) {
			$data['bildpay_geo_zone_id'] = $this->request->post['bildpay_geo_zone_id'];
		} else {
			$data['bildpay_geo_zone_id'] = $this->config->get('bildpay_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['bildpay_status'])) {
			$data['bildpay_status'] = $this->request->post['bildpay_status'];
		} else {
			$data['bildpay_status'] = $this->config->get('bildpay_status');
		}

		if (isset($this->request->post['bildpay_sort_order'])) {
			$data['bildpay_sort_order'] = $this->request->post['bildpay_sort_order'];
		} else {
			$data['bildpay_sort_order'] = $this->config->get('bildpay_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('payment/bildpay', $data));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/bildpay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['bildpay_merchant_id']) {
			$this->error['merchant_id'] = $this->language->get('error_merchant_id');
		}

		if (!$this->request->post['bildpay_private_key']) {
			$this->error['bildpay_private_key'] = $this->language->get('error_bildpay_private_key');
		}

		return !$this->error;
	}
}