<?php if ($testmode) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $text_testmode; ?></div>
<?php } ?>
<form action="<?php echo $action; ?>" method="post">
	<input type='hidden' name='-lang' value='en'>
	<input type='hidden' value='<?php echo $total_amount; ?>' name='PAYMENT_AMOUNT'>
	<input type='hidden' value='POST' name='PAYMENT_RESULT_METHOD'>
	<input type='hidden' value='<?php echo $notify_url; ?>' name='PAYMENT_RESULT_URL'>
	<input type='hidden' value='POST' name='PAYMENT_CANCEL_METHOD'>
	<input type='hidden' value='<?php echo $cancel_return; ?>' name='PAYMENT_CANCEL_URL'>
	<input type='hidden' value='<?php echo $currency_code; ?>' name='PAYMENT_CURRENCY'>
	<input type='hidden' value='Payment for goods' name='PAYMENT_DESC'>
	<input type='hidden' value='<?php echo $hash; ?>' name='PAYMENT_HASH'>
	<input type='hidden' value='<?php echo $merchant_id; ?>' name='PAYMENT_MERCHANT_ID'>
	<input type='hidden' value='<?php echo $order_id; ?>' name='PAYMENT_ORDER_NR'>
	<input type='hidden' value='POST' name='PAYMENT_SUCCESS_METHOD'>
	<input type='hidden' value='<?php echo $return; ?>' name='PAYMENT_SUCCESS_URL'>
	<input type='hidden' value='FD' name='PAYMENT_SYSTEM'>
  <div class="buttons">
    <div class="pull-right">
      <input type="submit" value="<?php echo $button_confirm; ?>" class="btn btn-primary" />
    </div>
  </div>
</form>
