<?php
class ControllerPaymentBildPay extends Controller {
	public function index() {
		$this->load->language('payment/bildpay');

		$data['text_testmode'] = $this->language->get('text_testmode');
		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['testmode'] = $this->config->get('bildpay_test');

		if (!$this->config->get('bildpay_test')) {
			$data['action'] = 'https://my.bilderlingspay.com/process/payment';
		} else {
			$data['action'] = 'https://secure-test.bilderlingspay.com/process/payment';
		}

		$this->load->model('checkout/order');
                             $this->load->model('localisation/country');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
                             $country_info = $this->model_localisation_country->getCountry($order_info['payment_country_id']);

		if ($order_info) {
			$data['merchant_id'] = $this->config->get('bildpay_merchant_id');
			$data['private_key'] = $this->config->get('bildpay_private_key');
			$data['item_name'] = html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

			$data['products'] = array();

			foreach ($this->cart->getProducts() as $product) {
				$option_data = array();

				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$data['products'][] = array(
					'name'     => htmlspecialchars($product['name']),
					'model'    => htmlspecialchars($product['model']),
					'price'    =>  $this->currency->format($product['price'], $order_info['currency_code'], false, false),
					'quantity' => $product['quantity'],
					'option'   => $option_data,
					'weight'   => $product['weight']
				);
			}

			$data['discount_amount_cart'] = 0;




                               $total2 = $this->currency->format($order_info['total']  - $this->cart->getSubTotal(), $order_info['currency_code'],  $order_info['currency_value'], false);

                               $total = $this->currency->format($order_info['total'] , $order_info['currency_code'],  $order_info['currency_value'], false);

			if ($total2 > 0) {
				$data['products'][] = array(
					'name'     => $this->language->get('text_total'),
					'model'    => '',
					'price'    => $total,
					'quantity' => 1,
					'option'   => array(),
					'weight'   => 0
				);
			} else {
				$data['discount_amount_cart'] -= $total2;
			}

			$data['order_id'] = "Order_".$order_info['order_id'];
			$data['total_amount'] = $total;
			$data['currency_code'] = $order_info['currency_code'];
			$data['first_name'] = html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8');
			$data['last_name'] = html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8');
			$data['address1'] = html_entity_decode($order_info['payment_address_1'], ENT_QUOTES, 'UTF-8');
			$data['address2'] = html_entity_decode($order_info['payment_address_2'], ENT_QUOTES, 'UTF-8');
			$data['city'] = html_entity_decode($order_info['payment_city'], ENT_QUOTES, 'UTF-8');
			$data['zip'] = html_entity_decode($order_info['payment_postcode'], ENT_QUOTES, 'UTF-8');
			$data['country'] = $order_info['payment_iso_code_2'];
			$data['email'] = $order_info['email'];
			$data['invoice'] = $this->session->data['order_id'] . ' - ' . html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8') . ' ' . html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8');
			$data['lc'] = $this->session->data['language'];
			$data['return'] = $this->url->link('checkout/success');
			//$data['notify_url'] = HTTPS_SERVER . 'index.php?route=payment/bildpay/callback';
			$data['notify_url'] = $this->url->link('payment/bildpay/callback', '', true);
			$data['cancel_return'] = $this->url->link('checkout/checkout', '', true);

			$data['hash'] = md5($data['merchant_id']."|".$data['order_id']."|".$data['total_amount']."|".$data['currency_code']."|"."FD"."|".$data['private_key']);

			$data['custom'] = $this->session->data['order_id'];

			return $this->load->view('payment/bildpay', $data);
		}
	}

	public function callback() {

                       $this->load->model('checkout/order');

		if (isset($this->request->post['ORDER_NR'])) {
			$a1 = $this->request->post['ORDER_NR'];
			$order_number = substr($a1,strpos($a1,"_")+1);
			$order_id = $order_number;
			// $this->log->write('ab:respn'.$this->request->post['ORDER_NR']);

			//$order_id = trim(substr(($this->request->post['orderid']), 6));
		} else {
			$order_id = 0;
		}

		// $this->log->write('bildpay :: RESPONSE ' . serialize($this->request->post));


		$order_info = $this->model_checkout_order->getOrder($order_id);

		if ($order_info) {

			if ($this->config->get('bildpay_debug')) {
				$this->log->write('bildpay :: IPN RESPONSE: ' . serialize($this->request->post));
			}

			if (isset($this->request->post['STATUS'])) {
				$order_status_id = $this->config->get('config_order_status_id');

				switch($this->request->post['STATUS']) {
					case '-1':
						$order_status_id = $this->config->get('bildpay_canceled_reversal_status_id');
						break;
					case '1':
						$order_status_id = $this->config->get('bildpay_completed_status_id');
						break;
					/*case 'Denied':
						$order_status_id = $this->config->get('bildpay_denied_status_id');
						break;
					case 'Expired':
						$order_status_id = $this->config->get('bildpay_expired_status_id');
						break;
					case 'Failed':
						$order_status_id = $this->config->get('bildpay_failed_status_id');
						break;
					case 'Pending':
						$order_status_id = $this->config->get('bildpay_pending_status_id');
						break;
					case 'Processed':
						$order_status_id = $this->config->get('bildpay_processed_status_id');
						break;
					case 'Refunded':
						$order_status_id = $this->config->get('bildpay_refunded_status_id');
						break;
					case 'Reversed':
						$order_status_id = $this->config->get('bildpay_reversed_status_id');
						break;
					case 'Voided':
						$order_status_id = $this->config->get('bildpay_voided_status_id');
						break;*/
                                                                     default: $order_status_id = $this->config->get('bildpay_canceled_reversal_status_id');
				}
                                                          //$this->model_checkout_order->confirm($order_id,2, 'CardPay payment was confirmed.', TRUE);
                                                         //  $this->model_checkout_order->update($order_id, 2, 'CardPay payment was confirmed.', TRUE);
                                                  //   $h23 = md5($data['merchant_id']."|".$data['order_id']."|".$data['total_amount']."|".$data['currency_code']."|"."FD"."|".$data['private_key']);

                                                           $total = $this->currency->format($order_info['total'] , $order_info['currency_code'],  $order_info['currency_value'], false);

				$b1 = $this->config->get('bildpay_merchant_id');
				$b2 = "Order_".$order_info['order_id'];
				$b3 =  number_format((float)$total , 2, '.', '');
			               $b4 =  $order_info['currency_code'];
			               $b5 = $this->config->get('bildpay_private_key');

			              $h25 = md5($b1."|".$b2."|".$b3."|".$b4."|"."FD"."|".$b5);


                                                $a1 = $this->request->post['MERCHANT_ID'];
                                                $a2 = $this->request->post['ORDER_NR'];
                                                $a3 = $this->request->post['AMOUNT'];
                                                $a4= $this->request->post['CURRENCY'];
                                                $a5 = $this->request->post['SYSTEM'];
                                                $a6 = $this->request->post['INVOICE_ID'];
                                                 $a7 = $this->request->post['TRANSFER_ID'];
                                                  $a8 = $this->request->post['STATUS'];
                                                   $a9 = $this->config->get('bildpay_private_key');



                                                   $b = $a1."|".$a2."|".$a3."|".$a4."|"."FD"."|".$a9;
                                      $c = md5($b);

                                      //$this->log->write('b:'.$b);
                                      //$this->log->write('c:'.$h25);
                                    //  $this->log->write('qq:'.$b1."|".$b2."|".$b3."|".$b4."|"."FD"."|".$b5);



                                               if($h25==$c){
                                                                 $this->model_checkout_order->addOrderHistory($order_id, $order_status_id );
                                               } else {

                                               	$this->model_checkout_order->addOrderHistory($order_id,  $this->config->get('bildpay_badsign_status_id'));
                                               	$this->log->write('Order failed:'.'Bad Signature'. 'Order #'.$a2);

                                                  $this->url->link('checkout/checkout', '', true);
                                               }

			} else {
				$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('bildpay_canceled_reversal_status_id'));
			}
		}
	}
}