<?php
class ControllerPaymentBildPay extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/bildpay');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('bildpay', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		//$this->data['text_edit'] = $this->language->get('text_edit');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');

		$this->data['entry_merchant_id'] = $this->language->get('entry_merchant_id');
		$this->data['entry_private_key'] = $this->language->get('entry_private_key');
		$this->data['entry_test'] = $this->language->get('entry_test');
		$this->data['entry_debug'] = $this->language->get('entry_debug');
		$this->data['entry_total'] = $this->language->get('entry_total');
		$this->data['entry_canceled_reversal_status'] = $this->language->get('entry_canceled_reversal_status');
		$this->data['entry_completed_status'] = $this->language->get('entry_completed_status');
		$this->data['badsign_status'] = $this->language->get('badsign');
		/*$this->data['entry_denied_status'] = $this->language->get('entry_denied_status');
		$this->data['entry_expired_status'] = $this->language->get('entry_expired_status');
		$this->data['entry_failed_status'] = $this->language->get('entry_failed_status');
		$this->data['entry_pending_status'] = $this->language->get('entry_pending_status');
		$this->data['entry_processed_status'] = $this->language->get('entry_processed_status');
		$this->data['entry_refunded_status'] = $this->language->get('entry_refunded_status');
		$this->data['entry_reversed_status'] = $this->language->get('entry_reversed_status');
		$this->data['entry_voided_status'] = $this->language->get('entry_voided_status');*/
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$this->data['help_test'] = $this->language->get('help_test');
		$this->data['help_debug'] = $this->language->get('help_debug');
		$this->data['help_total'] = $this->language->get('help_total');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_order_status'] = $this->language->get('tab_order_status');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['merchant_id'])) {
			$this->data['error_merchant_id'] = $this->error['merchant_id'];
		} else {
			$this->data['error_merchant_id'] = '';
		}

		if (isset($this->error['private_key'])) {
			$this->data['error_private_key'] = $this->error['private_key'];
		} else {
			$this->data['error_private_key'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payment'),
			'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/bildpay', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('payment/bildpay', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['bildpay_merchant_id'])) {
			$this->data['bildpay_merchant_id'] = $this->request->post['bildpay_merchant_id'];
		} else {
			$this->data['bildpay_merchant_id'] = $this->config->get('bildpay_merchant_id');
		}

		if (isset($this->request->post['bildpay_private_key'])) {
			$this->data['bildpay_private_key'] = $this->request->post['bildpay_private_key'];
		} else {
			$this->data['bildpay_private_key'] = $this->config->get('bildpay_private_key');
		}

		if (isset($this->request->post['bildpay_test'])) {
			$this->data['bildpay_test'] = $this->request->post['bildpay_test'];
		} else {
			$this->data['bildpay_test'] = $this->config->get('bildpay_test');
		}


		if (isset($this->request->post['bildpay_debug'])) {
			$this->data['bildpay_debug'] = $this->request->post['bildpay_debug'];
		} else {
			$this->data['bildpay_debug'] = $this->config->get('bildpay_debug');
		}

		if (isset($this->request->post['bildpay_total'])) {
			$this->data['bildpay_total'] = $this->request->post['bildpay_total'];
		} else {
			$this->data['bildpay_total'] = $this->config->get('bildpay_total');
		}

		if (isset($this->request->post['bildpay_canceled_reversal_status_id'])) {
			$this->data['bildpay_canceled_reversal_status_id'] = $this->request->post['bildpay_canceled_reversal_status_id'];
		} else {
			$this->data['bildpay_canceled_reversal_status_id'] = $this->config->get('bildpay_canceled_reversal_status_id');
		}

		if (isset($this->request->post['bildpay_completed_status_id'])) {
			$this->data['bildpay_completed_status_id'] = $this->request->post['bildpay_completed_status_id'];
		} else {
			$this->data['bildpay_completed_status_id'] = $this->config->get('bildpay_completed_status_id');
		}

		if (isset($this->request->post['bildpay_badsign_status_id'])) {
			$this->data['bildpay_badsign_status_id'] = $this->request->post['bildpay_badsign_status_id'];
		} else {
			$this->data['bildpay_badsign_status_id'] = $this->config->get('bildpay_badsign_status_id');
		}

		/*if (isset($this->request->post['bildpay_denied_status_id'])) {
			$this->data['bildpay_denied_status_id'] = $this->request->post['bildpay_denied_status_id'];
		} else {
			$this->data['bildpay_denied_status_id'] = $this->config->get('bildpay_denied_status_id');
		}

		if (isset($this->request->post['bildpay_expired_status_id'])) {
			$this->data['bildpay_expired_status_id'] = $this->request->post['bildpay_expired_status_id'];
		} else {
			$this->data['bildpay_expired_status_id'] = $this->config->get('bildpay_expired_status_id');
		}

		if (isset($this->request->post['bildpay_failed_status_id'])) {
			$this->data['bildpay_failed_status_id'] = $this->request->post['bildpay_failed_status_id'];
		} else {
			$this->data['bildpay_failed_status_id'] = $this->config->get('bildpay_failed_status_id');
		}

		if (isset($this->request->post['bildpay_pending_status_id'])) {
			$this->data['bildpay_pending_status_id'] = $this->request->post['bildpay_pending_status_id'];
		} else {
			$this->data['bildpay_pending_status_id'] = $this->config->get('bildpay_pending_status_id');
		}

		if (isset($this->request->post['bildpay_processed_status_id'])) {
			$this->data['bildpay_processed_status_id'] = $this->request->post['bildpay_processed_status_id'];
		} else {
			$this->data['bildpay_processed_status_id'] = $this->config->get('bildpay_processed_status_id');
		}

		if (isset($this->request->post['bildpay_refunded_status_id'])) {
			$this->data['bildpay_refunded_status_id'] = $this->request->post['bildpay_refunded_status_id'];
		} else {
			$this->data['bildpay_refunded_status_id'] = $this->config->get('bildpay_refunded_status_id');
		}

		if (isset($this->request->post['bildpay_reversed_status_id'])) {
			$this->data['bildpay_reversed_status_id'] = $this->request->post['bildpay_reversed_status_id'];
		} else {
			$this->data['bildpay_reversed_status_id'] = $this->config->get('bildpay_reversed_status_id');
		}

		if (isset($this->request->post['bildpay_voided_status_id'])) {
			$this->data['bildpay_voided_status_id'] = $this->request->post['bildpay_voided_status_id'];
		} else {
			$this->data['bildpay_voided_status_id'] = $this->config->get('bildpay_voided_status_id');
		}*/

		$this->load->model('localisation/order_status');

		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['bildpay_geo_zone_id'])) {
			$this->data['bildpay_geo_zone_id'] = $this->request->post['bildpay_geo_zone_id'];
		} else {
			$this->data['bildpay_geo_zone_id'] = $this->config->get('bildpay_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['bildpay_status'])) {
			$this->data['bildpay_status'] = $this->request->post['bildpay_status'];
		} else {
			$this->data['bildpay_status'] = $this->config->get('bildpay_status');
		}

		if (isset($this->request->post['bildpay_sort_order'])) {
			$this->data['bildpay_sort_order'] = $this->request->post['bildpay_sort_order'];
		} else {
			$this->data['bildpay_sort_order'] = $this->config->get('bildpay_sort_order');
		}

		$this->template = 'payment/bildpay.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/bildpay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['bildpay_merchant_id']) {
			$this->error['merchant_id'] = $this->language->get('error_merchant_id');
		}

		if (!$this->request->post['bildpay_private_key']) {
			$this->error['bildpay_private_key'] = $this->language->get('error_bildpay_private_key');
		}

		return !$this->error;
	}
}