<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
  <style>
.tooltip {
    position: relative;
    display: inline-block;
    }

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 1s;
}

.tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
}
</style>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
    <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
   <div class="heading">
      <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
     <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
      <tr>
            <h2> <?php echo $tab_general; ?> </h2>
           
             </tr>
          <tr>
            <td><span class="required">*</span> <?php echo $entry_merchant_id; ?></td>
            <td> <input type="text" name="bildpay_merchant_id" value="<?php echo $bildpay_merchant_id; ?>" placeholder="<?php echo $entry_merchant_id; ?>" id="entry-merchant_id"/>
              <?php if ($error_merchant_id) { ?>
              <span class="error"><?php echo $error_merchant_id; ?></span>
              <?php } ?></td>
             </tr>
           <tr>
            <td><span class="required">*</span> <?php echo $entry_private_key; ?></td>
            <td> <input type="text" name="bildpay_private_key" value="<?php echo $bildpay_private_key; ?>" placeholder="<?php echo $entry_private_key; ?>" id="entry-private_key" class="form-control"/>
              <?php if ($error_private_key) { ?>
              <span class="error"><?php echo $error_private_key; ?></span>
              <?php } ?></td>
             </tr>
          <tr>
            <td><span class="tooltip"><?php echo $entry_test; ?><span class="tooltiptext"><?php echo $help_test; ?></span></span></td>
            <td> <select name="bildpay_test" id="input-live-demo" class="form-control">
                    <?php if ($bildpay_test) { ?>
                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                    <option value="0"><?php echo $text_no; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_yes; ?></option>
                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                    <?php } ?>
                  </select></td>
             </tr>
         <tr>
            <td><span class="tooltip"><?php echo $entry_debug; ?><span class="tooltiptext"><?php echo $help_debug; ?></span></span></td>
            <td> <select name="bildpay_debug" id="input-debug" class="form-control">
                    <?php if ($bildpay_debug) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select></td>
             </tr>
          <tr>
            <td><span class="tooltip"><?php echo $entry_total; ?><span class="tooltiptext"><?php echo $help_total;?></span></span></td>
            <td>  <input type="text" name="bildpay_total" value="<?php echo $bildpay_total; ?>" placeholder="<?php echo $entry_total; ?>" id="input-total" class="form-control"/>
        </td>
             </tr>
          <tr>
            <td> <?php echo $entry_geo_zone;?></td>
            <td> <select name="bildpay_geo_zone_id" id="input-geo-zone" class="form-control">
                    <option value="0"><?php echo $text_all_zones; ?></option>
                    <?php foreach ($geo_zones as $geo_zone) { ?>
                    <?php if ($geo_zone['geo_zone_id'] == $bildpay_geo_zone_id) { ?>
                    <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select></td>
             </tr>
         <tr>
            <td> <?php echo $entry_status;?></td>
            <td> <select name="bildpay_status" id="input-status" class="form-control">
                    <?php if ($bildpay_status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select></td>
             </tr>
           <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td>   <input type="text" name="bildpay_sort_order" value="<?php echo $bildpay_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control"/>
        </td>
             </tr>
             <tr><td style="border: medium none;">
            <h2 > <?php echo $tab_order_status; ?></h2>
           </td>
             </tr>
         
          <tr>
            <td><?php echo $entry_canceled_reversal_status;?></td>
            <td>  <select name="bildpay_canceled_reversal_status_id" id="input-canceled-reversal-status" class="form-control">
                    <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $bildpay_canceled_reversal_status_id) { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
        </td>
             </tr>
            <tr>
            <td><?php echo $entry_completed_status;?></td>
            <td>   <select name="bildpay_completed_status_id" id="input-completed-status" class="form-control">
                    <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $bildpay_completed_status_id) { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
        </td>
             </tr>
          <tr>
            <td><?php echo $badsign_status;?></td>
            <td>   <select name="bildpay_badsign_status_id" id="input-badsign-status" class="form-control">
                    <?php foreach ($order_statuses as $order_status) { ?>
                    <?php if ($order_status['order_status_id'] == $bildpay_badsign_status_id) { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
        </td>
             </tr>
        </table>
      </form>
  </div>  
    
</div>
</div>
<?php echo $footer; ?>